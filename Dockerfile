ARG from=ros:noetic-perception-focal
FROM ${from}

RUN apt-get update &&\
    apt-get install -y \
        net-tools \
        iputils-ping \
        dnsutils \
        git \
        ros-noetic-moveit\
        ros-noetic-moveit-visual-tools\
        ros-noetic-rosserial-python \
        ros-noetic-robot-state-publisher \
    &&\
    rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod 755 /usr/local/bin/entrypoint.sh
RUN mkdir -p /catkin_ws/src

WORKDIR /catkin_ws/src
RUN git clone https://github.com/billieblaze/moveo_ros.git

WORKDIR /catkin_ws
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["roscore"]
